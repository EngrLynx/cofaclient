angular.module('app.controllers', [])
  
.controller('coFACtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
      
.controller('menuCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('setNewGoalCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {


}])
   
.controller('getDummyGoalCtrl', ['$scope', '$stateParams', 'GoalService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, GoalService) {
    $scope.getGoal = function() {
        $scope.purpose = "Processing";
        
        GoalService.getGoal(1).then(function(res) {
            $scope.purpose = res.purpose;  
            $scope.amount = res.amount;  
        });
    }
}])
   
.controller('loginCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {
    $scope.login = function() {
        var data = $.param({
            email: $scope.email,
            password: $scope.password
        });        
        var config = {
            headers : {
                'Content-Type': 'application/json;charset=utf-8;'
            }
        }

        $http.post('http://192.168.0.11:3000/api/Customers/login', data, config)
        .success(function (data, status, headers, config) {
            $scope.email = "Success";
        })
        .error(function (data, status, header, config) {
            $scope.email = "";
        })
    }
}])
 