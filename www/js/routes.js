angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('tabsController.coFA', {
    url: '/CoFA',
    views: {
      'tab1': {
        templateUrl: 'templates/coFA.html',
        controller: 'coFACtrl'
      }
    }
  })

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

  .state('tabsController.setNewGoal', {
    url: '/setGoal',
    views: {
      'tab1': {
        templateUrl: 'templates/setNewGoal.html',
        controller: 'setNewGoalCtrl'
      }
    }
  })

  .state('tabsController.getDummyGoal', {
    url: '/getDummyGoal',
    views: {
      'tab1': {
        templateUrl: 'templates/getDummyGoal.html',
        controller: 'getDummyGoalCtrl'
      }
    }
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

$urlRouterProvider.otherwise('/login')

  

});