angular.module('app.services', [])

.factory('GoalService', ['$http', '$q',function($http, $q){
    
    return {
        getGoal: function(id) {
            var deferred = $q.defer();
            
            console.log('testing' + id);
            $http.get('http://192.168.0.11:3000/api/Goals/' + id)
            .then(
            function (response) {
                console.log('success');
                deferred.resolve(response.data);
            }, function(response) {
                console.log('error');
                var errMsg = {
                    purpose: "Error",
                };
                deferred.resolve(errMsg);
            });

            return deferred.promise;
        }
    };
}])

.service('BlankService', [function(){

}]);